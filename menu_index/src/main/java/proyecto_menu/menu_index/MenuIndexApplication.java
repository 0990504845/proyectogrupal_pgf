package proyecto_menu.menu_index;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenuIndexApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenuIndexApplication.class, args);
	}

}
