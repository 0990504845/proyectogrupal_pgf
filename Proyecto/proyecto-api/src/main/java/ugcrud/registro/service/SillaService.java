package ugcrud.registro.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ugcrud.registro.models.registro.silla;
import ugcrud.registro.models.registro.sillaRepositorio;




@Service
public class SillaService {
    
    private final sillaRepositorio sillaRepository;

    public SillaService(sillaRepositorio sillaRepository) {
        this.sillaRepository = sillaRepository;
    }

    public Iterable<silla> save(List<silla> sillas) {
        return sillaRepository.saveAll(sillas);
    }
}

