package ugcrud.registro.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ugcrud.registro.models.registro.Periferico;
import ugcrud.registro.models.registro.PerifericoRepositorio;


@Service
public class PerifericoService {
    
    private final PerifericoRepositorio perifericoRepository;

    public PerifericoService(PerifericoRepositorio perifericoRepository) {
        this.perifericoRepository = perifericoRepository;
    }

    public Iterable<Periferico> save(List<Periferico> perifericos) {
        return perifericoRepository.saveAll(perifericos);
    }
}

