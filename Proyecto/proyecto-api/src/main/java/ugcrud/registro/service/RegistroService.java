package ugcrud.registro.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ugcrud.registro.models.registro.Registro;
import ugcrud.registro.models.registro.RegistroRepositorio;


@Service
public class RegistroService {
    
    private final RegistroRepositorio registroRepository;

    public RegistroService(RegistroRepositorio registroRepository) {
        this.registroRepository = registroRepository;
    }

    public Iterable<Registro> save(List<Registro> registros) {
        return registroRepository.saveAll(registros);
    }
}
