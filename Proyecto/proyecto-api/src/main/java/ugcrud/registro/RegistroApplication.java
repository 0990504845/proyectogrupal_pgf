package ugcrud.registro;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ugcrud.registro.models.registro.Periferico;
import ugcrud.registro.models.registro.Registro;
import ugcrud.registro.models.registro.silla;
import ugcrud.registro.service.PerifericoService;
import ugcrud.registro.service.RegistroService;
import ugcrud.registro.service.SillaService;



@SpringBootApplication
public class RegistroApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(RegistroApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(RegistroService registroService) {
		return args -> {
			ObjectMapper mapper =  new ObjectMapper();
			TypeReference<List<Registro>> typeReference = new TypeReference<List<Registro>>(){};
			String file = "/static/registro.json";
			InputStream inputStream = new ClassPathResource(file).getInputStream();
			try {
				List<Registro> Registros = mapper.readValue(inputStream,typeReference);
				registroService.save(Registros);				
			} catch (IOException e) {
				System.out.println("No se pudo guardar registros");
			}
		};
	}
	@Bean
	CommandLineRunner runner2(PerifericoService perifericoService) {
		return args -> {
			ObjectMapper mapper =  new ObjectMapper();
			TypeReference<List<Periferico>> typeReference = new TypeReference<List<Periferico>>(){};
			String file = "/static/periferico.json";
			InputStream inputStream = new ClassPathResource(file).getInputStream();
			try {
				List<Periferico> Perifericos = mapper.readValue(inputStream,typeReference);
				perifericoService.save(Perifericos);				
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		};
	}
	@Bean
	CommandLineRunner runner3(SillaService sillaService) {
		return args -> {
			ObjectMapper mapper =  new ObjectMapper();
			TypeReference<List<silla>> typeReference = new TypeReference<List<silla>>(){};
			String file = "/static/silla.json";
			InputStream inputStream = new ClassPathResource(file).getInputStream();
			try {
				List<silla> sillas = mapper.readValue(inputStream,typeReference);
				sillaService.save(sillas);				
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		};
	}
}
