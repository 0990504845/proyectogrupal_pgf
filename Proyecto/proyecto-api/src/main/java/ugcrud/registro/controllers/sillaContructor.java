package ugcrud.registro.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ugcrud.registro.models.registro.silla;
import ugcrud.registro.models.registro.sillaRepositorio;

@RestController
@RequestMapping("/sillas")
class sillaContructor {

    @Autowired
    sillaRepositorio repository;

    @GetMapping
    public ResponseEntity<List<silla>> getAll() {
        try {
            List<silla> items = new ArrayList<silla>();

            repository.findAll().forEach(items::add);

            if (items.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            return new ResponseEntity<>(items, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<silla> getById(@PathVariable("id") Long id) {
        Optional<silla> existingItemOptional = repository.findById(id);

        if (existingItemOptional.isPresent()) {
            return new ResponseEntity<>(existingItemOptional.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<silla> create(@RequestBody silla item) {
        try {
            silla savedItem = repository.save(item);
            return new ResponseEntity<>(savedItem, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<silla> update(@PathVariable("id") Long id, @RequestBody silla item) {
        Optional<silla> existingItemOptional = repository.findById(id);
        if (existingItemOptional.isPresent()) {
            silla existingItem = existingItemOptional.get();
            System.out.println("TODO for developer - update logic is unique to entity and must be implemented manually.");
            //existingItem.setSomeField(item.getSomeField());
            existingItem.setNombre(item.getNombre());
            existingItem.setEdad(item.getEdad());
            existingItem.setSexo(item.getSexo());
            existingItem.setPregunt1(item.getPregunt1());
            existingItem.setPregunt2(item.getPregunt2());
            existingItem.setPregunt3(item.getPregunt3());
            existingItem.setPregunt4(item.getPregunt4());
            existingItem.setPregunt5(item.getPregunt5());
            return new ResponseEntity<>(repository.save(existingItem), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
        try {
            repository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}