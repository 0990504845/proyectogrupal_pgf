package ugcrud.registro.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ugcrud.registro.models.registro.Registro;
import ugcrud.registro.models.registro.RegistroRepositorio;

@RestController
@RequestMapping("/registros")
class RegistroController {

    @Autowired
    RegistroRepositorio repository;

    @GetMapping
    public ResponseEntity<List<Registro>> getAll() {
        try {
            List<Registro> items = new ArrayList<Registro>();

            repository.findAll().forEach(items::add);

            if (items.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            return new ResponseEntity<>(items, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<Registro> getById(@PathVariable("id") Long id) {
        Optional<Registro> existingItemOptional = repository.findById(id);

        if (existingItemOptional.isPresent()) {
            return new ResponseEntity<>(existingItemOptional.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Registro> create(@RequestBody Registro item) {
        try {
            Registro savedItem = repository.save(item);
            return new ResponseEntity<>(savedItem, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Registro> update(@PathVariable("id") Long id, @RequestBody Registro item) {
        Optional<Registro> existingItemOptional = repository.findById(id);
        if (existingItemOptional.isPresent()) {
            Registro existingItem = existingItemOptional.get();
            
            existingItem.setNombre(item.getNombre());
            existingItem.setApellido(item.getApellido());
            existingItem.setEmail(item.getEmail());
            existingItem.setInstitucion(item.getInstitucion());
            existingItem.setContrasena(item.getContrasena());

            return new ResponseEntity<>(repository.save(existingItem), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
        try {
            repository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}