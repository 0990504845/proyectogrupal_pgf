package ug.registrowebui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ug.registrowebui.cliente.PerifericoCliente;
import ug.registrowebui.entidad.Periferico;






@Controller
public class PerifericoController {

    

    @GetMapping("/periferico")
    public String getPerifericos(Model model) {
        PerifericoCliente pc = new PerifericoCliente();
        model.addAttribute("perifericos",pc.getList());
        return "periferico";
    }

    @GetMapping("/showaddperiferico")
    public String showaddperiferico(Periferico periferico) {
        return "periferico-add";
    }


    @PostMapping(value="/addperiferico")
    public String addPeriferico(@ModelAttribute("periferico") Periferico periferico) {
        PerifericoCliente pc = new PerifericoCliente();
        pc.add(periferico);     
        return "redirect:/periferico";
    }

    
}