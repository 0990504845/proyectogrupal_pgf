package ug.registrowebui.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ug.registrowebui.cliente.SillaCliente;
import ug.registrowebui.entidad.Silla;




@Controller
public class SillaController {

    @GetMapping("/silla")
    public String getSillas(Model model) {
        SillaCliente pc = new SillaCliente();
        model.addAttribute("sillas",pc.getList());
        return "silla";
    }

    @GetMapping("/showaddsilla")
    public String showaddsilla(Silla silla) {
        return "silla-add";
    }

    @PostMapping(value="/addsilla")
    public String addSilla(@ModelAttribute("silla") Silla silla) {
        SillaCliente pc = new SillaCliente();
        pc.add(silla);     
        return "redirect:/silla";
    }
    
}
