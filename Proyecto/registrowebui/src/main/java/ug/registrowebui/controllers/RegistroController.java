package ug.registrowebui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


import ug.registrowebui.cliente.RegistroCliente;
import ug.registrowebui.entidad.Registro;


@Controller
public class RegistroController {

    @GetMapping("/menu")
    public String getMenu(Model model) {
        return "menu";
    }
    @GetMapping("/evaluacion")
    public String getEva(Model model) {
        return "evaluacion";
    }

    
    @GetMapping("/registro")
    public String getRegistros(Model model) {
        RegistroCliente pc = new RegistroCliente();
        model.addAttribute("registros",pc.getList());
        return "registro";
    }

    @GetMapping("/showaddregistro")
    public String showAddRegistro(Registro registro) {
        return "registro-add";
    }

    @GetMapping("/showUpdateRegistro/{id}")
    public String showUpdateRegistro(@PathVariable(value = "id") long id, Model model) {
        RegistroCliente pc = new RegistroCliente();
        Registro registro = pc.getRegistroById(id);
        model.addAttribute("registro",registro);
        return "registro-update";  
    }

    @PostMapping(value="/addregistro")
    public String addRegistro(@ModelAttribute("registro") Registro registro) {
        RegistroCliente pc = new RegistroCliente();
        pc.add(registro);     
        return "redirect:/registro";
    }

    @PostMapping(value="/updateRegistro/{id}")
    public String updateRegistro(@PathVariable(value = "id") long id,@ModelAttribute("registro") Registro registro) {
        RegistroCliente pc = new RegistroCliente();
        pc.update(id,registro);
        return "redirect:/registro";
    }

    @GetMapping(value="/deleteRegistro/{id}")
    public String deleteRegistro(@PathVariable(value = "id") long id, Model model) {
        RegistroCliente pc = new RegistroCliente();
        pc.delete(id);
        return "redirect:/registro";
    }    
    
}
