package ug.registrowebui.cliente;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import ug.registrowebui.entidad.Registro;

public class RegistroCliente {
    private final RestTemplate restTemplate = new RestTemplate();

    public List<Registro> getList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<List<Registro>> responseEntity = 
            restTemplate.exchange(Global.URL_REGISTRO,
            HttpMethod.GET, requestEntity, 
            new ParameterizedTypeReference<List<Registro>>() {} );

        List<Registro> registros = responseEntity.getBody();
        return registros;
    }

    public Registro getRegistroById(long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<Registro> responseEntity = 
            restTemplate.exchange(Global.URL_REGISTRO.concat("/{id}"),
            HttpMethod.GET, requestEntity, Registro.class, id );
        
        Registro registro =  responseEntity.getBody();
        return registro;
    }

    public Registro add(Registro registro) {
        int status = HttpStatus.OK.value();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Registro> requestEntity =  new HttpEntity<>(registro,headers);
        Registro registroNuevo = null;
        try {
            registroNuevo = restTemplate.postForEntity(Global.URL_REGISTRO,
                requestEntity, Registro.class).getBody();
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || 
                HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
                    status = httpClientOrServerExc.getRawStatusCode();
                }
        }
        return registroNuevo;
    }

    public Integer update(long id, Registro registro) {
        int status = HttpStatus.OK.value();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Registro> requestEntity =  new HttpEntity<>(registro,headers);
        try {
            restTemplate.put(Global.URL_REGISTRO.concat("/{id}"),requestEntity,id);            
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || 
                HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
                    status = httpClientOrServerExc.getRawStatusCode();
                }
        }
        return status;
    }

    public Integer delete(long id) {
        int status = HttpStatus.NO_CONTENT.value(); 

        restTemplate.delete(Global.URL_REGISTRO.concat("/{id}"),id);
        return status;
    }
}
