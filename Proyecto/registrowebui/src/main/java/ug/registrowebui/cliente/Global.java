package ug.registrowebui.cliente;

public class Global {
    public static String URL = "http://localhost:8090";
    public static String URL_REGISTRO = URL.concat("/registros");
    public static String URL_PERIFERICO = URL.concat("/perifericos");
    public static String URL_SILLA = URL.concat("/sillas");
}
