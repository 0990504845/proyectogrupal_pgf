package ug.registrowebui.cliente;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import ug.registrowebui.entidad.Periferico;


public class PerifericoCliente {
    private final RestTemplate restTemplate = new RestTemplate();

    public List<Periferico> getList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<List<Periferico>> responseEntity = 
            restTemplate.exchange(Global.URL_PERIFERICO,
            HttpMethod.GET, requestEntity, 
            new ParameterizedTypeReference<List<Periferico>>() {} );

        List<Periferico> perifericos = responseEntity.getBody();
        return perifericos;
    }

    public Periferico getPerifericoById(long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<Periferico> responseEntity = 
            restTemplate.exchange(Global.URL_PERIFERICO.concat("/{id}"),
            HttpMethod.GET, requestEntity, Periferico.class, id );
        
        Periferico periferico =  responseEntity.getBody();
        return periferico;
    }

    public Periferico add(Periferico periferico) {
        int status = HttpStatus.OK.value();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Periferico> requestEntity =  new HttpEntity<>(periferico,headers);
        Periferico perifericoNuevo = null;
        try {
            perifericoNuevo = restTemplate.postForEntity(Global.URL_PERIFERICO,
                requestEntity, Periferico.class).getBody();
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || 
                HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
                    status = httpClientOrServerExc.getRawStatusCode();
                }
        }
        return perifericoNuevo;
    }

    public Integer update(long id, Periferico periferico) {
        int status = HttpStatus.OK.value();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Periferico> requestEntity =  new HttpEntity<>(periferico,headers);
        try {
            restTemplate.put(Global.URL_PERIFERICO.concat("/{id}"),requestEntity,id);            
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || 
                HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
                    status = httpClientOrServerExc.getRawStatusCode();
                }
        }
        return status;
    }

    public Integer delete(long id) {
        int status = HttpStatus.NO_CONTENT.value(); 

        restTemplate.delete(Global.URL_PERIFERICO.concat("/{id}"),id);
        return status;
    }
}
