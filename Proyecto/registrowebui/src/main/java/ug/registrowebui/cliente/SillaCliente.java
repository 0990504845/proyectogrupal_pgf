package ug.registrowebui.cliente;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import ug.registrowebui.entidad.Silla;



public class SillaCliente {
    private final RestTemplate restTemplate = new RestTemplate();

    public List<Silla> getList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<List<Silla>> responseEntity = 
            restTemplate.exchange(Global.URL_SILLA,
            HttpMethod.GET, requestEntity, 
            new ParameterizedTypeReference<List<Silla>>() {} );

        List<Silla> sillas = responseEntity.getBody();
        return sillas;
    }

    public Silla add(Silla silla) {
        int status = HttpStatus.OK.value();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Silla> requestEntity =  new HttpEntity<>(silla,headers);
        Silla sillaNuevo = null;
        try {
            sillaNuevo = restTemplate.postForEntity(Global.URL_SILLA,
                requestEntity, Silla.class).getBody();
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || 
                HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
                    status = httpClientOrServerExc.getRawStatusCode();
                }
        }
        return sillaNuevo;
    }
}
