package ug.registrowebui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrowebuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrowebuiApplication.class, args);
	}

}
