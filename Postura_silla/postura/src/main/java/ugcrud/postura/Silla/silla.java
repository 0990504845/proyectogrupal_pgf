package ugcrud.postura.Silla;
// ISAAC FlORES
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
class silla {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String pregunt1;
    private String pregunt2;
    private String pregunt3;
    private String pregunt4;
    private String pregunt5;

    public silla() {
    }

    

    public silla(Long id, String pregunt1, String pregunt2, String pregunt3, String pregunt4, String pregunt5) {
        this.id = id;
        this.pregunt1 = pregunt1;
        this.pregunt2 = pregunt2;
        this.pregunt3 = pregunt3;
        this.pregunt4 = pregunt4;
        this.pregunt5 = pregunt5;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getPregunt1() {
        return pregunt1;
    }



    public void setPregunt1(String pregunt1) {
        this.pregunt1 = pregunt1;
    }



    public String getPregunt2() {
        return pregunt2;
    }



    public void setPregunt2(String pregunt2) {
        this.pregunt2 = pregunt2;
    }



    public String getPregunt3() {
        return pregunt3;
    }



    public void setPregunt3(String pregunt3) {
        this.pregunt3 = pregunt3;
    }



    public String getPregunt4() {
        return pregunt4;
    }



    public void setPregunt4(String pregunt4) {
        this.pregunt4 = pregunt4;
    }



    public String getPregunt5() {
        return pregunt5;
    }



    public void setPregunt5(String pregunt5) {
        this.pregunt5 = pregunt5;
    }

    
}