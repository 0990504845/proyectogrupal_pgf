package ugcrud.postura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PosturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PosturaApplication.class, args);
	}

}
